package com.recommender.main;

import java.util.Scanner;

import com.recommender.logic.RecommendationEngine;

public class Main
{
	public static void main( String[] args )
	{
		while( true )
		{
			Integer userID = null;
			try
			{
				Scanner in = new Scanner( System.in );
				System.out.println( "Please enter a user ID to get recommendations : " );
				String s = in.nextLine();
				userID = new Integer( s );
			}
			catch( NumberFormatException e )
			{
				System.out.println( "Please enter a valid integer number" + "\n" );
				continue;
			}
			catch( Exception e )
			{
				e.printStackTrace();
			}
			RecommendationEngine engine = new RecommendationEngine();
			System.out.println( "Product ID Recommendations in Descending order : " );
			if( engine.isValidUserID( userID ) )
			{
				System.out.println( engine.recommendationsToString( engine.getRecommendation( userID ) ) + "\n" );
			}
			else
			{
				System.out.println( "Sorry the user ID you entered is not in our database, Please try again with another user ID" + "\n" );
			}
		}
	}

}
