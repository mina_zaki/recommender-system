package com.recommender.models;

import java.util.HashMap;

public abstract class Matrix
{

	private HashMap<Integer, EntryList> matrix;

	public Matrix()
	{
		this.matrix = new HashMap<Integer, EntryList>();
	}

	public HashMap<Integer, EntryList> getMatrix(){
		return this.matrix;
	}
	
	public void setMatrix(){
		this.matrix = new HashMap<Integer, EntryList>(  );
	}
	
	public abstract void addRecord( Integer userID, Integer key, Integer value );

	public String toString()
	{
		String result = "";
		for( java.util.Map.Entry<Integer, EntryList> entry : matrix.entrySet() )
		{
			result += entry.getKey().intValue() + ":" + entry.getValue().toString() + "\n";
		}
		return result;
	}

}
