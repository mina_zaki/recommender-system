package com.recommender.models;

public class Entry implements Comparable<Entry>
{
	private Integer key;
	private Integer value;
	
	public Integer getKey()
	{
		return key;
	}

	public Integer getValue()
	{
		return value;
	}

	public Entry(Integer key, Integer value){
		this.key = key;
		this.value = value;
	}

	public String toString(){
		// TODO rewrite toString
		return this.key.toString();
	}
	
	@Override
	public int compareTo( Entry o )
	{
		
		return this.value.compareTo( o.value );
	}

}
