package com.recommender.models;

import java.util.ArrayList;

public class EntryList
{
	private ArrayList<Entry> values;

	public ArrayList<Entry> getValues()
	{
		return values;
	}

	public EntryList()
	{
		this.values = new ArrayList<Entry>();
	}
	public ArrayList<Integer> getValuesAsIntegers(){
		ArrayList<Integer> result = new ArrayList<Integer>();
		for(Entry entry : values){
			result.add( entry.getKey() );
		}
		return result;
	}
	public void put( Integer k, Integer v )
	{
		values.add( new Entry( k, v ) );
	}

	public String toString()
	{
		String result = "";		
		for(Entry entry : values)
		{
			
			result += "(" + entry.getKey().intValue() + "," + entry.getValue().intValue() + ")";
		}
		return result;
	}

	public Integer calculateScore( EntryList o )
	{
		int score = 0;
		for(Entry compare : this.values)
		{
			
			for( Entry comparedTo : o.values)
			{				
				if( compare.getKey().compareTo( comparedTo.getKey()) == 0 )
				{
					score += 1;
				}
			}
		}
		return new Integer( score );
	}
}
