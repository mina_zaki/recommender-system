package com.recommender.models;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class InputMatrix extends Matrix
{	
	private Set<Integer> productIDs;

	public InputMatrix(){
		super();
		this.productIDs = new HashSet<Integer>( );		
	}
	
	public  void addRecord( Integer userID, Integer productID, Integer value )
	{
		if( !super.getMatrix().containsKey( userID ) )
		{
			super.getMatrix().put( userID, new EntryList() );
		}
		super.getMatrix().get( userID ).put( productID, value );
		this.productIDs.add( productID );
	}
	
	public Set<Integer> getProductIDs(){
		return this.productIDs;
	}

	public ArrayList<Integer> getProductIDs(Integer userID){
		return super.getMatrix().get( userID ).getValuesAsIntegers();
	}
}
