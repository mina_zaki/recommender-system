package com.recommender.models;

import java.util.ArrayList;

public class RecommendationMatrix extends Matrix
{


	public RecommendationMatrix(){
		super();
	}
	
	public  void addRecord( Integer userID1, Integer userID2, Integer score)
	{
		if( !super.getMatrix().containsKey( userID1 ) )
		{
			super.getMatrix().put( userID1, new EntryList() );
		}
		super.getMatrix().get( userID1 ).put( userID2, score);
		
		if( !super.getMatrix().containsKey( userID2 ) )
		{
			super.getMatrix().put( userID2, new EntryList() );
		}
		super.getMatrix().get( userID2 ).put( userID1, score);
	}
	
	public ArrayList<Integer> getProductIDs(Integer userID, InputMatrix inputMatrix){		
		return inputMatrix.getProductIDs( userID );		
	}
}
