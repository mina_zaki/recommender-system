package com.recommender.logic;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.recommender.models.Matrix;

public class Parser
{
	private String filePath;

	public Parser( String filePath )
	{
		this.filePath = filePath;
	}

	public void parseFileToMatrix( Matrix matrix )
	{

		BufferedReader br;
		try
		{
			br = new BufferedReader( new FileReader( this.filePath ) );
			String currentLine;

			while( (currentLine = br.readLine()) != null )
			{
				String[] line = getIdsFromString( currentLine );
				matrix.addRecord( Integer.parseInt( line[0] ), Integer.parseInt( line[1] ), new Integer( 1 ) );
			}
		}
		catch( FileNotFoundException e )
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch( IOException e )
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch( NumberFormatException e )
		{
			e.printStackTrace();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	private static String[] getIdsFromString( String s )
	{
		String[] output = new String[2];
		output = s.split( ";" );
		return output;
	}

}
