package com.recommender.logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Set;

import com.recommender.models.Entry;
import com.recommender.models.EntryList;
import com.recommender.models.InputMatrix;
import com.recommender.models.RecommendationMatrix;

public class RecommendationEngine
{
	private RecommendationMatrix recommendationMatrix;
	private InputMatrix inputMatrix;

	public RecommendationEngine()
	{
		this.recommendationMatrix = new RecommendationMatrix();
		this.inputMatrix = new InputMatrix();
		Parser p = new Parser( "lib/recommendation.txt" );
		p.parseFileToMatrix( this.inputMatrix );
		calculateRecommendations();
		System.out.println( recommendationMatrix.toString() );
		System.out.println( inputMatrix.toString() );
	}

	private void calculateRecommendations()
	{
		HashMap<Integer, EntryList> matrix = inputMatrix.getMatrix();
		ArrayList<Integer> keyList = new ArrayList<Integer>( matrix.keySet() );
		for( Integer firstKey : keyList )
		{
			ArrayList<Integer> subList = new ArrayList<Integer>( keyList.subList( keyList.indexOf( firstKey ) + 1, keyList.size() ) );
			for( Integer secondKey : subList )
			{
				recommendationMatrix.addRecord( firstKey, secondKey, matrix.get( firstKey ).calculateScore( matrix.get( secondKey ) ) );
			}
		}
	}

	public Set<Integer> getRecommendation( Integer userID )
	{
		Set<Integer> result = new LinkedHashSet<Integer>();
		EntryList recommendations = recommendationMatrix.getMatrix().get( userID );
		ArrayList<Entry> userIDs = recommendations.getValues();
		Collections.sort( userIDs, Collections.reverseOrder() );
		for( Entry id : userIDs )
		{
			result.addAll( recommendationMatrix.getProductIDs( id.getKey(), inputMatrix ) );
		}
		return result;
	}

	public boolean isValidUserID( Integer userID )
	{
		return this.inputMatrix.getMatrix().containsKey( userID );
	}

	public String recommendationsToString( Set<Integer> recommendations )
	{
		String result = "";
		for( Integer integ : recommendations )
		{
			result += integ.intValue() + ", ";
		}
		return result.substring( 0, result.length() - 2 );
	}
}
